/**
 * @class Animator
 */
class Animator {

    /**
     * @constructor
     *
     * @param el HTMLElement
     * @param {object|object[]} options - Animation properties and keyframes
     */
    constructor(el, options) {
        this._init(el, options);
    }

    /**
     * CSS keyframes name
     *
     * @return {string}
     */
    static get cssKeyframePrefix() {
        return '@keyframes';
    }

    /**
     * Get default id prefix
     *
     * @return {string} Default id prefix
     */
    static get cssIdPrefix() {
        return 'animator_';
    }

    /**
     * Get default css class prefix
     *
     * @return {string} Default class prefix
     */
    static get cssDefaultClassPrefix() {
        return 'animator-';
    }

    /**
     * Get Animation properties
     *
     * @return {object[]}
     */
    static get cssAnimationProperties() {
        return ['name', 'duration', 'timingFunction', 'delay', 'iterationCount', 'direction', 'fillMode', 'playState'];
    }

    /**
     * Exception function
     *
     * @static
     * @private
     *
     * @param message
     */
    static _exception(message) {
        throw new Error(`Animator: ${message}`)
    }

    /**
     * Generate unique Id
     *
     * @static
     * @private
     *
     * @return {string} Unique Id
     */
    static getUniqueId() {
        return Math.random().toString(36).substring(2);
    }

    /**
     * Initialization
     *
     * @private
     *
     * @param el HTMLElement
     * @param {object|object[]} options - Animation properties and keyframes
     *
     */
    _init(el, options) {

        if (typeof el === 'string') {
            this.element = document.querySelector(el);
        } else if (el instanceof HTMLElement) {
            this.element = el;
        }

        if (!this.element) {
            Animator._exception(`Expected 'HTMLElement' element or 'string' query, but got ${typeof el}`);
        }

        if (Object.prototype.toString.call(options) === "[object Object]") {
            options = [options];
        }

        if (!Array.isArray(options)) {
            Animator._exception('Provide right options.');
        }

        this.id = Animator.getUniqueId();

        if (!this.element.id) {
            this.element.id = Animator.cssIdPrefix + this.id;
        }

        this.element.classList.add(Animator.cssDefaultClassPrefix + this.id);

        this.computedStyle = getComputedStyle(this.element);

        Animator._insertIntoHtml(this._buildCssStylesheets(options));
    }

    /**
     * Build CSS styles and insert into html head
     *
     * @private
     *
     * @param {object|object[]} options - Animation properties and keyframes
     * @return {string} Style element with animation styles
     */
    _buildCssStylesheets(options) {

        const length = options.length - 1;

        let keyframes = '', animation = 'animation: ';

        options.forEach((option, i) => {

            if (!option.name) {
                Animator._exception('Keyframes name is missing!');
            }

            if (!option.keyframes) {
                Animator._exception('Keyframes options are missing!');
            }

            animation += Animator._parseAnimationStyles(option, length === i);
            keyframes += Animator._parseKeyFrames(option);
        });

        return `.${Animator.cssDefaultClassPrefix + this.id} {${animation}} ${keyframes}`;
    }

    /**
     * Parse keyframes object to CSS string
     *
     * @static
     * @private
     *
     * @param {object} options Keyframe options object
     *
     * @returns {string} CSS string
     */
    static _parseKeyFrames(options) {

        const keyframes = options.keyframes;

        const keyframeList = Object.keys(keyframes).reduce((style, key) => {
            return style + ` ${key} { ${Animator._parseObjectToCssProperties(keyframes[key])} }`;
        }, '');

        return `${Animator.cssKeyframePrefix} ${options.name} { ${keyframeList} }`;
    }

    /**
     * Parse animation object to CSS animation
     *
     * @static
     * @private
     *
     * @param {object} options - Animation properties object
     * @param {boolean} end
     *
     * @returns {string} CSS animation string
     */
    static _parseAnimationStyles(options, end = true) {

        let str = Animator.cssAnimationProperties.reduce((style, key) => {
            return style + (options[key] ? ` ${options[key]}` : '');
        }, '');

        str += !end ? ',' : ';';

        return str;
    }

    /**
     * Insert CSS string into Html head
     *
     * @static
     * @private
     *
     * @param {string} styles - CSS animation styles
     *
     * @returns {Element}
     */
    static _insertIntoHtml(styles) {
        let styleElement = document.createElement('style');
        styleElement.innerHTML = styles;

        document.head.appendChild(styleElement);

        return styleElement;
    }

    /**
     * Parse object to CSS properties
     *
     * @static
     * @private
     *
     * @param {object} properties - CSS properties
     *
     * @returns {string} CSS properties
     */
    static _parseObjectToCssProperties(properties) {
        return Object.keys(properties)
            .reduce((style, key) => style + `${key}: ${properties[key]};`, '');
    }

    /**
     * Play the animation
     *
     * @public
     *
     * @returns {object} Instance
     */
    play() {
        this.element.style.animationPlayState = 'running';
        return this;
    }

    /**
     * Pause the animation
     *
     * @public
     *
     * @returns {object} Instance
     */
    pause() {
        this.element.style.animationPlayState = 'paused';
        return this;
    }

    /**
     * Toggle animating state
     *
     * @public
     *
     * @returns {object} Instance
     */
    toggle() {
        return this.computedStyle.animationPlayState != 'running'
            ? this.play() :
            this.pause();
    }

    /**
     * Point to a specific state
     *
     * @public
     *
     * @param value
     * @return {object} Instance
     */
    seek(value) {
        //ToDo: 'ms' should be dynamic.
        this.element.style.animationDelay = value + 'ms';
        return this;
    }
}